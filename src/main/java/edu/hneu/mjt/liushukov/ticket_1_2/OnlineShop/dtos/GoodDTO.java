package edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.dtos;

import edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.models.Supplier;
import lombok.Data;

@Data
public class GoodDTO {
    private Long id;
    private String name;
    private String description;
    private double price;
    private String category;
    private Supplier supplier;
}
