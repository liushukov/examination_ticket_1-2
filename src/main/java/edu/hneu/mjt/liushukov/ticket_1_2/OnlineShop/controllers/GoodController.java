package edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.controllers;

import edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.dtos.GoodDTO;
import edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.models.Good;
import edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.services.GoodServiceImpl;
import edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.services.SupplierServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("good")
public class GoodController {
    private final GoodServiceImpl goodService;
    private final SupplierServiceImpl supplierService;

    public GoodController(GoodServiceImpl goodService, SupplierServiceImpl supplierService) {
        this.goodService = goodService;
        this.supplierService = supplierService;
    }
    @GetMapping("/all")
    public ResponseEntity<List<Good>> goods(){
        return ResponseEntity.ok(goodService.getAllGoods());
    }
    @GetMapping("/{id}")
    public ResponseEntity<Good> good(@PathVariable("id") Long id){
        return ResponseEntity.ok(goodService.getGoodById(id));
    }
    @PostMapping("/add/{id}")
    public ResponseEntity<Good> addGood(@PathVariable("id") Long id, @RequestBody GoodDTO goodDTO){
        var supplier = supplierService.getSupplierById(id);
        if (supplier != null){
            var good = new Good(
                    goodDTO.getName(),
                    goodDTO.getDescription(),
                    goodDTO.getPrice(),
                    goodDTO.getCategory(),
                    supplier
            );
            goodService.createGood(good);
            return ResponseEntity.ok(good);
        }
        return ResponseEntity.notFound().build();
    }
    @PutMapping("/editGood/{id}")
    public ResponseEntity<Good> editGood(@PathVariable("id") Long id, @RequestBody GoodDTO goodDTO){
        var existedGood = goodService.getGoodById(id);
        if (existedGood != null){
            existedGood.setName(goodDTO.getName());
            existedGood.setDescription(goodDTO.getDescription());
            existedGood.setPrice(goodDTO.getPrice());
            existedGood.setCategory(goodDTO.getCategory());
            goodService.updateGood(existedGood);
            return ResponseEntity.ok(existedGood);
        }
        return ResponseEntity.notFound().build();
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteGood(@PathVariable("id") Long id){
        var existedGood = goodService.getGoodById(id);
        String response = "";
        if (existedGood != null){
            goodService.deleteGoodById(id);
            response = "Successfuly deleted good by id";
        } else {response = "Good doesn't exist";}
        return ResponseEntity.ok(response);
    }
}
