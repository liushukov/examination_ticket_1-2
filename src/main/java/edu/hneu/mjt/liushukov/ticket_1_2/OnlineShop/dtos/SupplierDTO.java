package edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.dtos;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
public class SupplierDTO {
    private long id;
    private String companyName;
    private String country;
    @Pattern(regexp = "\\+38\\(0\\d{2}\\) \\d{3}-\\d{2}-\\d{2}", message = "Phone number must be in the format +38(0xx) xxx-xx-xx")
    private String contactNumber;
    private String address;
    @Pattern(regexp = "https://t.me/[a-zA-Z0-9_]+", message = "Messenger must be a valid Telegram link")
    private String messenger;
}
