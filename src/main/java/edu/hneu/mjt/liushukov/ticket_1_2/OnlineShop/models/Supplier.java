package edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "suppliers")
public class Supplier {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "companyName")
    private String companyName;
    @Column(name = "country")
    private String country;
    @Column(name = "contactNumber")
    private String contactNumber;
    @Column(name = "address")
    private String address;
    @Column(name = "messenger")
    private String messenger;
    @JsonIgnore
    @OneToMany(mappedBy = "supplier", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Good> cards;

    public Supplier(String companyName, String country, String contactNumber, String address, String messenger) {
        this.companyName = companyName;
        this.country = country;
        this.contactNumber = contactNumber;
        this.address = address;
        this.messenger = messenger;
    }
}
