package edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.controllers;

import edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.dtos.SupplierDTO;
import edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.models.Supplier;
import edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.services.SupplierServiceImpl;
import org.springframework.http.ResponseEntity;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("supplier")
public class SupplierController {
    private final SupplierServiceImpl supplierService;

    public SupplierController(SupplierServiceImpl supplierService) {
        this.supplierService = supplierService;
    }
    @GetMapping("/all")
    public ResponseEntity<List<Supplier>> suppliers(){
        return ResponseEntity.ok(supplierService.getAllSuppliers());
    }
    @GetMapping("/{id}")
    public ResponseEntity<Supplier> supplier(@PathVariable("id") Long id){
        return ResponseEntity.ok(supplierService.getSupplierById(id));
    }
    @PostMapping("/add")
    public ResponseEntity<Supplier> addSupplier(@Valid @RequestBody SupplierDTO supplierDTO){
        var supplier = new Supplier(
                supplierDTO.getCompanyName(),
                supplierDTO.getCountry(),
                supplierDTO.getContactNumber(),
                supplierDTO.getAddress(),
                supplierDTO.getMessenger()
        );
        var response = supplierService.createSupplier(supplier);
        return ResponseEntity.ok(response);
    }
    @PutMapping("/edit/{id}")
    public ResponseEntity<Supplier> updateSupplier(@PathVariable("id") Long id, @Valid @RequestBody SupplierDTO supplierDTO){
        var existedSupplier = supplierService.getSupplierById(id);
        if (existedSupplier != null){
            existedSupplier.setCompanyName(supplierDTO.getCompanyName());
            existedSupplier.setCountry(supplierDTO.getCountry());
            existedSupplier.setContactNumber(supplierDTO.getContactNumber());
            existedSupplier.setAddress(supplierDTO.getAddress());
            existedSupplier.setMessenger(supplierDTO.getMessenger());
            var response = supplierService.updateSupplier(existedSupplier);
            return ResponseEntity.ok(response);
        }
        return ResponseEntity.notFound().build();
    }
    @DeleteMapping("/delete/{id}")
    public String deleteSupplier(@PathVariable("id") Long id){
        var existedSupplier = supplierService.getSupplierById(id);
        String response = "";
        if (existedSupplier != null){
            supplierService.deleteSupplier(id);
            response = "Successfuly deleted account by id";
        } else {response = "Account did't find";}
        return response;
    }
}
