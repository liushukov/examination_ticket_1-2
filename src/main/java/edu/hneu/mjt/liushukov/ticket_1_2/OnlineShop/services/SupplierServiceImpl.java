package edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.services;

import edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.interfaces.SupplierService;
import edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.models.Supplier;
import edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.repositories.SupplierRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SupplierServiceImpl implements SupplierService {
    private final SupplierRepository supplierRepository;

    public SupplierServiceImpl(SupplierRepository supplierRepository) {
        this.supplierRepository = supplierRepository;
    }

    @Override
    public List<Supplier> getAllSuppliers() {
        return supplierRepository.findAll();
    }

    @Override
    public Supplier getSupplierById(long id) {
        return supplierRepository.findById(id).orElse(null);
    }

    @Override
    public Supplier createSupplier(Supplier supplier) {
        return supplierRepository.save(supplier);
    }

    @Override
    public Supplier updateSupplier(Supplier supplier) {
        return supplierRepository.save(supplier);
    }

    @Override
    public void deleteSupplier(long id) {
        supplierRepository.deleteById(id);
    }
}
