package edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.interfaces;

import edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.models.Supplier;

import java.util.List;

public interface SupplierService {
    public List<Supplier> getAllSuppliers();
    public Supplier getSupplierById(long id);
    public Supplier createSupplier(Supplier supplier);
    public Supplier updateSupplier(Supplier supplier);
    public void deleteSupplier(long id);
}
