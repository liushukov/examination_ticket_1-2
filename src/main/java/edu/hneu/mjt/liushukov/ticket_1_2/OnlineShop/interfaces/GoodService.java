package edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.interfaces;

import edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.models.Good;

import java.util.List;

public interface GoodService {
    public Good getGoodById(long id);
    public List<Good> getAllGoods();
    public Good createGood(Good good);
    public Good updateGood(Good good);
    public void deleteGoodById(long id);
}
