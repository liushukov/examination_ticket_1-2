package edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.services;

import edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.interfaces.GoodService;
import edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.models.Good;
import edu.hneu.mjt.liushukov.ticket_1_2.OnlineShop.repositories.GoodRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodServiceImpl implements GoodService {
    private final GoodRepository goodRepository;

    public GoodServiceImpl(GoodRepository goodRepository) {
        this.goodRepository = goodRepository;
    }

    @Override
    public Good getGoodById(long id) {
        return goodRepository.findById(id).orElse(null);
    }

    @Override
    public List<Good> getAllGoods() {
        return goodRepository.findAll();
    }

    @Override
    public Good createGood(Good good) {
        return goodRepository.save(good);
    }

    @Override
    public Good updateGood(Good good) {
        return goodRepository.save(good);
    }

    @Override
    public void deleteGoodById(long id) {
        goodRepository.deleteById(id);
    }
}
